#! /bin/bash

#This function prints information about CPU
function processor {
 echo -e "\033[31m~~~~~~~~~~ PROCESSOR ~~~~~~~~~~\033[0m"
 echo ""processors"      : "`cat /proc/cpuinfo | grep -c "processor"`""
 echo "`cat /proc/cpuinfo | grep "vendor_id" | head -1`"
 echo "`cat /proc/cpuinfo | grep "model name" | head -1`"
}
#This function prints information about the system
function system {
 echo -e "\033[33m~~~~~~~~~~~ SYSTEM ~~~~~~~~~~~\033[0m"
 echo "system name     : `cat /etc/*release* | grep "DISTRIB_DESCRIPTION" |cut -d"=" -f2`"
 echo "digit capacity  : `uname -m`"
 echo "core version    : `uname -r`"
}
#This function prints out information about memory
function memory {
 echo -e "\033[32m~~~~~~~~~~~ MEMORY ~~~~~~~~~~~\033[0m"
 TOTAL="`free -h | grep "Mem"`"
 echo "total memory    : `echo $TOTAL |cut -d" " -f2`"
 echo "used memory     : `echo $TOTAL |cut -d" " -f3`"
 echo "free memory     : `echo $TOTAL |cut -d" " -f4`"
}
#This function prints information about the hard disk
function hdd {
 echo -e "\033[34m~~~~~~~~~~~~ HDD ~~~~~~~~~~~~\033[0m"
 echo "NAME     SIZE"
 echo "`lsblk -o NAME,SIZE | grep "sd"`"
}
#Function gives information about the display
function display {
 echo -e "\033[36m~~~~~~~~~~ DISPLAY ~~~~~~~~~~\033[0m"
 echo "`xrandr | grep -v "disconnected"`"
 echo "`xwininfo -root | head -n12 | tail -n2`"
 echo "`lspci |grep "VGA"`"
}
#This function prints the network information
function network {
 echo -e "\033[35m~~~~~~~~~~ NETWORK ~~~~~~~~~~\033[0m"
 ETH=`ifconfig`
 for i in $ETH
  do
   if [ $i == "eth0" ]
    then
     echo "ethernet        : YES"
    break
  fi
 done
 WLAN=`ifconfig`
 for j in $WLAN
  do
   if [ $j == "wlan0" ]
    then
     echo "WiFi            : YES"
    break
  fi
 done
}
#Function gives information about the program
function Help {
 echo " --all :all information"
 echo " -p    :processor information"
 echo " -s    :system information"
 echo " -m    :memory information"
 echo " -h    :HDD information"
 echo " -d    :display information"
 echo " -n    :network information"
}
if [ -z "$*"]
then
  Help
fi
#By argument gives the necessary information
for i in $*
 do
if [ $i == "--all" ]
 then
  processor
  system
  memory
  hdd
  display
  network
 elif [ "$i" == " --help" ]
  then
   Help
 elif [ "$i" == "-p" ]
  then
   processor
 elif [ "$i" == "-s" ]
  then
   system
 elif [ "$i" == "-m" ]
  then
   memory
 elif [ "$i" == "-h" ]
  then
   hdd
 elif [ "$i" == "-n" ]
  then
   network
 elif [ "$i" == "-d" ]
  then
   display
 else
  Help
fi
done
