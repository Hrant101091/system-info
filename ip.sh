#! /bin/bash

#It shows the IP addresses of the computers connected to the network
QANAK=0
for I in `seq 1 30`
do
A="192.168.1.$I"
C=`ping -c1 -w1 $A|grep "1 received"`
if [ -n "$C" ]
 then 
echo -e "\033[31m$A\033[0m" 
QANAK=`expr $QANAK + 1`
fi
done
echo -e "\033[35;1m Count = $QANAK\033[0m" 
